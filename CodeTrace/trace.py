from datetime import datetime
import sys

class CodeTrace:
	def log(str):
			sys.stderr.write(str)

	def trace(quiet=False, skip=False):
		def trace_decorator(func):
			def func_wrapper(*args, **kwargs):
				if(not skip):
					CodeTrace.log("[" + str(datetime.now()) + "] ")
					CodeTrace.log(str(func.__module__) + " *** ENTER " + func.__name__)
					if not quiet:
						CodeTrace.log("(" + str(args[1]) + ")")
					CodeTrace.log("\n")
				ret = func(*args, **kwargs)
				if(not skip):
					CodeTrace.log("[" + str(datetime.now()) + "] ")
					CodeTrace.log(str(func.__module__) + " *** EXIT  " + func.__name__)
					if not quiet:
						CodeTrace.log("(" + str(ret) + ")")
					CodeTrace.log("\n")
				return ret
			return func_wrapper
		return trace_decorator
