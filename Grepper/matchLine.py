import re
from CodeTrace import CodeTrace

class Grepper:
	@CodeTrace.trace(skip=True)
	def __init__(self, regex):
		self.regex = self._set_regex(regex)

	@CodeTrace.trace() # quiet=True)
	def _set_regex(self, regex):
		# A literal string, not a regular expression, so escape special chars.
		# After this point, we need to double-escape non-word characters.
		regex = re.escape(regex)

		# Anchor either end of the string so that it must match exactly.
		regex = re.sub(r"^(.+)$", r"^\1$", regex)

		# If we get `%{\d}`, it needs to match one or more chars.
		# Don't be greedy for this one.
		regex = re.sub(r"\\\%\\\{\d+\\\}", r".+?", regex)

		# With `%{\dG}`, match one or more chars and be greedy
		regex = re.sub(r"\\\%\\\{\d+G\\\}", r".+", regex)

		# With `%{\dS\d}`, we have to match exactly N whitespace chars
		# between text literals
		regex = re.sub(r"\\\%\\\{\d+S(\d+)\\\}", r"(?:\S+\s){\1}\S+", regex)

		return regex

	@CodeTrace.trace() # quiet=True)
	def matchLine(self, line):
		m = re.match(self.regex, line)
		if(m):
			return True
		return False
