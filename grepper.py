import sys
import Grepper

def main():
	regexes = sys.argv[1:]
	for line in sys.stdin:
		for regex in regexes:
			g = Grepper.Grepper(regex)
			if(g.matchLine(line.rstrip())):
				print(line)

if __name__ == "__main__":
	main()
