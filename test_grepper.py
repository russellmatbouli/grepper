import pytest
import Grepper

regexes = [
	'hello',
	'world',
	'hello %{1}',
	'a %{2} is a %{3}',
	'this %{4S0} could %{5S0}',
	'another %{6G} test',
	'some %{7S2}'
	]
tests = [
	('hello', True),
	('something', False),
	('world', True),
	('fail', False),
	('hello there', True),
	('hello there sir', True),
	('a cat is a dog', True),
	('a is a man', False),
	('a kitty cat is a pet', True),
	('this man could run', True),
	('this super man could fly', False),
	('another flippin great test', True),
	('some more testing here', True),
	('some more  here', False)
	]
for t in tests:
	(test, expected) = t
	r = False
	for regex in regexes:
		g = Grepper.Grepper(regex)
		res = g.matchLine(test)
		r = r or res
		print(str(r))
	assert r == expected
